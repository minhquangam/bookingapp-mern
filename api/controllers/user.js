import Users from "../models/Users.js";

export const updateUser = async (req, res) => {
  try {
    const updatedUsers = await Users.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(updatedUsers);
  } catch (error) {
    next(error);
  }
};

export const deleteUser = async (req, res) => {
  try {
    await Users.findByIdAndDelete(req.params.id);
    res.status(200).json("Delete completed!");
  } catch (error) {
    next(error);
  }
};

export const getUsers = async (req, res, next) => {
  try {
    const users = await Users.find(req.params.id);
    res.status(200).json(users);
  } catch (error) {
    next(error);
  }
};

export const getUser = async (req, res, next) => {
  try {
    const users = await Users.findById(req.params.id);
    res.status(200).json(users);
  } catch (error) {
    next(error);
  }
}
